package me.taucu.commandblocker.util.text;

public class SlashUtil {

    public static String apply(String command) {
        // For some reason double slash commands include both slashes while single slash commands include nothing.
        // Sense it makes not.
        // Additionally, below <1.13 this will be called with an empty string and should reply with one
        return !command.isEmpty() ? (command.charAt(0) == '/' ? command : '/' + command) : command;
    }

}
