package me.taucu.commandblocker.util.logging;

import net.md_5.bungee.api.ChatColor;

import java.util.logging.Level;
import java.util.logging.LogRecord;

public class SimpleChatColorLogFormatter extends SimpleLogFormatter {

    public static final SimpleLogFormatter INSTANCE = new SimpleChatColorLogFormatter();
    public static final SimpleLogFormatter INSTANCE_NEWLINE = new SimpleChatColorLogFormatter();

    private final ChatColor defaultColor, warnColor, severeColor;

    public SimpleChatColorLogFormatter() {
        this(ChatColor.GRAY, ChatColor.YELLOW, ChatColor.RED);
    }

    public SimpleChatColorLogFormatter(ChatColor defaultColor, ChatColor warnColor, ChatColor severeColor) {
        this(false, defaultColor, warnColor, severeColor);
    }

    public SimpleChatColorLogFormatter(boolean newline, ChatColor defaultColor, ChatColor warnColor, ChatColor severeColor) {
        super(newline);
        this.defaultColor = defaultColor;
        this.warnColor = warnColor;
        this.severeColor = severeColor;
    }

    @Override
    public String format(LogRecord record) {
        Level level = record.getLevel();
        ChatColor color;
        switch (level.intValue()) {
            case 900:
                color = warnColor;
                break;
            case 1000:
                color = severeColor;
                break;
            default:
                color = defaultColor;
                break;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(color + "[");
        sb.append(record.getLevel().getLocalizedName());
        sb.append("]: [");
        sb.append(record.getLoggerName());
        sb.append("] ");
        sb.append(record.getMessage());
        if (newline) {
            sb.append('\n');
        }
        return sb.toString();
    }
}
