package me.taucu.commandblocker.platform.velocity;

import com.velocitypowered.api.proxy.Player;
import me.taucu.commandblocker.User;

public class VelocityUser implements User {

    public final Player player;

    public VelocityUser(Player player) {
        this.player = player;
    }

    @Override
    public boolean hasPermission(String permission) {
        return player.hasPermission(permission);
    }

    @Override
    public Object getKey() {
        return player.getUniqueId();
    }

    @Override
    public Object getContext() {
        return player.getCurrentServer().orElse(null);
    }

}
