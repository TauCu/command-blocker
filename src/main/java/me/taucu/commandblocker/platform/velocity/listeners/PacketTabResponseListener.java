package me.taucu.commandblocker.platform.velocity.listeners;

import com.velocitypowered.proxy.protocol.packet.TabCompleteResponsePacket;
import dev.simplix.protocolize.api.Direction;
import dev.simplix.protocolize.api.listener.PacketReceiveEvent;
import dev.simplix.protocolize.api.listener.PacketSendEvent;
import me.taucu.commandblocker.CommandBlocker;
import me.taucu.commandblocker.User;
import me.taucu.commandblocker.filters.Filters;
import me.taucu.commandblocker.platform.velocity.VelocityUser;
import me.taucu.commandblocker.util.TabMemory;
import me.taucu.commandblocker.util.packets.ProtocolizePacketListener;
import me.taucu.commandblocker.util.text.SlashUtil;

import java.util.Iterator;

public class PacketTabResponseListener extends ProtocolizePacketListener<TabCompleteResponsePacket> {

    final CommandBlocker pl;
    final Filters filters;

    public PacketTabResponseListener(CommandBlocker pl) {
        super(TabCompleteResponsePacket.class, Direction.UPSTREAM, 0);
        this.pl = pl;
        this.filters = pl.getFilters();
    }

    @Override
    public void packetReceive(PacketReceiveEvent<TabCompleteResponsePacket> packetReceiveEvent) {}

    @Override
    public void packetSend(PacketSendEvent<TabCompleteResponsePacket> e) {
        TabCompleteResponsePacket res = e.packet();
        TabMemory mem = TabMemory.MEMORY.get(e.player().uniqueId());
        if (mem != null) {
            if (res.getTransactionId() == mem.transactionId && mem.request.length() >= res.getStart()) {
                User u = new VelocityUser(e.player().handle());
                // res#start() is always zero <1.13 causing request command to be blank.
                // This is okay as the response will reply with the full command suggestion
                String requestCommand = mem.request.substring(0, res.getStart());
                Iterator<TabCompleteResponsePacket.Offer> it = res.getOffers().iterator();
                while (it.hasNext()) {
                    if (filters.apply(u, SlashUtil.apply(requestCommand + it.next().getText())) != null) {
                        it.remove();
                    }
                }

                if (!res.getOffers().isEmpty()) {
                    return; //valid responses
                }
            }
        }
        e.cancelled(true); //nonsense or empty responses
    }

}
