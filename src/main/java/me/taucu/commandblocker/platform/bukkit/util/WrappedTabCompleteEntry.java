package me.taucu.commandblocker.platform.bukkit.util;

import com.comphenix.protocol.reflect.FuzzyReflection;
import com.comphenix.protocol.utility.MinecraftReflection;
import com.comphenix.protocol.wrappers.WrappedChatComponent;

import java.lang.reflect.Field;

public class WrappedTabCompleteEntry {

    private static Field TEXT_FIELD;

    private final Object handle;

    public WrappedTabCompleteEntry(Object handle) {
        this.handle = handle;
    }

    public String getText() {
        try {
            if (TEXT_FIELD == null) {
                Field field = FuzzyReflection.fromObject(handle, true)
                        .getFieldByType("text", String.class);
                field.setAccessible(true);
                TEXT_FIELD = field;
            }
            return (String) TEXT_FIELD.get(handle);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public WrappedChatComponent getTooltip() {
        try {
            Field tooltipField = FuzzyReflection.fromObject(handle, true)
                    .getFieldByType("tooltip", MinecraftReflection.getChatComponentTextClass());
            tooltipField.setAccessible(true);
            return WrappedChatComponent.fromHandle(tooltipField.get(handle));
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public Object getHandle() {
        return handle;
    }

}
