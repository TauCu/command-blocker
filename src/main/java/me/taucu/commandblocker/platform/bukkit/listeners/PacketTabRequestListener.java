package me.taucu.commandblocker.platform.bukkit.listeners;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import me.taucu.commandblocker.filters.Filters;
import me.taucu.commandblocker.platform.bukkit.BukkitCommandBlocker;
import me.taucu.commandblocker.platform.bukkit.BukkitUser;
import me.taucu.commandblocker.util.TabMemory;
import me.taucu.commandblocker.util.text.SlashUtil;

public class PacketTabRequestListener extends PacketAdapter {

    final Filters filters;

    public PacketTabRequestListener(BukkitCommandBlocker plugin) {
        super(PacketAdapter.params(plugin, PacketType.Play.Client.TAB_COMPLETE).optionAsync());
        this.filters = plugin.getFilters();
    }

    @Override
    public void onPacketReceiving(PacketEvent e) {
        PacketContainer packet = e.getPacket();
        int transactionId = packet.getIntegers().read(0);
        String command = packet.getStrings().read(0);

        // As of writing this VIA breaks double slash commands... oh well. Not my job.
        if (filters.apply(new BukkitUser(e.getPlayer()), SlashUtil.apply(command)) != null) {
            e.setCancelled(true);
        }

        // always redefine to ensure an atomic, consistent state
        TabMemory.MEMORY.put(e.getPlayer().getUniqueId(), new TabMemory(transactionId, command));
    }

    public PacketTabRequestListener register() {
        ProtocolLibrary.getProtocolManager().addPacketListener(this);
        return this;
    }

    public PacketTabRequestListener unregister() {
        ProtocolLibrary.getProtocolManager().removePacketListener(this);
        return this;
    }

}
