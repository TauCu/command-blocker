package me.taucu.commandblocker.platform.bukkit.listeners;

import me.taucu.commandblocker.filters.AbstractFilter;
import me.taucu.commandblocker.filters.Filters;
import me.taucu.commandblocker.platform.bukkit.BukkitCommandBlocker;
import me.taucu.commandblocker.platform.bukkit.BukkitUser;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class CommandListener implements Listener {
    
    private final Filters filters;
    
    public CommandListener(BukkitCommandBlocker pl) {
        this.filters = pl.getFilters();
    }
    
    @EventHandler
    public void onChat(PlayerCommandPreprocessEvent e) {
        AbstractFilter filter = filters.apply(new BukkitUser(e.getPlayer()), e.getMessage());
        if (filter != null) {
            e.setCancelled(true);
            if (filter.shouldSendDenyMessage()) {
                e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', filter.getDenyMsg().replace("%command%", e.getMessage())));
            }
        }
    }
    
}
