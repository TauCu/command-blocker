package me.taucu.commandblocker.util.logging;

import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public class SimpleLogFormatter extends Formatter {

    public static final SimpleLogFormatter INSTANCE = new SimpleLogFormatter(false);
    public static final SimpleLogFormatter INSTANCE_NEWLINE = new SimpleLogFormatter(true);

    protected final boolean newline;

    public SimpleLogFormatter(boolean newline) {
        this.newline = newline;
    }

    @Override
    public String format(LogRecord record) {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append(record.getLevel().getLocalizedName());
        sb.append("]: [");
        sb.append(record.getLoggerName());
        sb.append("] ");
        sb.append(record.getMessage());
        if (newline) {
            sb.append('\n');
        }
        return sb.toString();
    }

    public String resolveLevel(LogRecord record) {
        return record.getLevel().getLocalizedName();
    }

}
