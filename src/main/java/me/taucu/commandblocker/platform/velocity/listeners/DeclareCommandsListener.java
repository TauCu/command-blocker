package me.taucu.commandblocker.platform.velocity.listeners;

import com.mojang.brigadier.tree.CommandNode;
import com.mojang.brigadier.tree.RootCommandNode;
import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.command.PlayerAvailableCommandsEvent;
import me.taucu.commandblocker.User;
import me.taucu.commandblocker.filters.Filters;
import me.taucu.commandblocker.platform.velocity.VelocityCommandBlocker;
import me.taucu.commandblocker.platform.velocity.VelocityUser;

import java.util.Iterator;

public class DeclareCommandsListener {

    public final Filters filters;

    public DeclareCommandsListener(VelocityCommandBlocker plugin) {
        this.filters = plugin.getFilters();
    }

    @Subscribe
    public void onCommandSend(PlayerAvailableCommandsEvent e) {
        RootCommandNode<?> root = e.getRootNode();
        if (root != null) {
            filterNodes(new VelocityUser(e.getPlayer()), root);
        }
    }

    private void filterNodes(User u, CommandNode<?> node) {
        Iterator<? extends CommandNode<?>> it = node.getChildren().iterator();
        while (it.hasNext()) {
            node = it.next();
            if (filters.apply(u, '/' + node.getName()) != null) {
                it.remove();
            }
        }
    }

}
