package me.taucu.commandblocker.platform;

import me.taucu.commandblocker.CommandBlocker;
import me.taucu.commandblocker.ConfigLoader;
import me.taucu.commandblocker.filters.Filters;

import java.io.File;
import java.util.Properties;
import java.util.logging.Logger;

public abstract class AbstractCommandBlocker implements CommandBlocker {

    protected final Logger logger;

    protected final Properties pomProperties = new Properties();
    protected final Properties pluginProperties = new Properties();

    protected final ConfigLoader configLoader;
    protected final File dataDirectory;

    protected final Filters filters = new Filters();

    public AbstractCommandBlocker(Logger log, File dataDirectory, String configFileName) {
        this.logger = log;
        this.dataDirectory = dataDirectory;

        loadInternalProperties("META-INF/maven/me.taucu/commandblocker/pom.properties", pomProperties);
        loadInternalProperties("command-blocker.properties", pluginProperties);

        this.configLoader = new ConfigLoader(dataDirectory, configFileName, log, filters);
    }

    @Override
    public File getDataDirectory() {
        return dataDirectory;
    }

    @Override
    public ConfigLoader getConfigLoader() {
        return configLoader;
    }

    @Override
    public Filters getFilters() {
        return filters;
    }

    @Override
    public Logger getLogger() {
        return logger;
    }

    @Override
    public String getPluginVersion() {
        return pomProperties.getProperty("version", "could not retrieve version from pom.properties");
    }

    @Override
    public Properties getPluginProperties() {
        return pluginProperties;
    }

}
