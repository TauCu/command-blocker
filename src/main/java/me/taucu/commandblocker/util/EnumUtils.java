package me.taucu.commandblocker.util;

public class EnumUtils {
    
    public static <T extends Enum<T>> T valueOf(Class<T> t, String s) {
        try {
            return Enum.valueOf(t, s);
        } catch (IllegalArgumentException a) {
            return null;
        }
    }
    
}
