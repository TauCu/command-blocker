package me.taucu.commandblocker.platform.bungeecord.listeners;

import dev.simplix.protocolize.api.Direction;
import dev.simplix.protocolize.api.listener.PacketReceiveEvent;
import dev.simplix.protocolize.api.listener.PacketSendEvent;
import me.taucu.commandblocker.CommandBlocker;
import me.taucu.commandblocker.filters.Filters;
import me.taucu.commandblocker.platform.bungeecord.BungeeUser;
import me.taucu.commandblocker.util.TabMemory;
import me.taucu.commandblocker.util.packets.ProtocolizePacketListener;
import me.taucu.commandblocker.util.text.SlashUtil;
import net.md_5.bungee.protocol.packet.TabCompleteRequest;

public class PacketTabRequestListener extends ProtocolizePacketListener<TabCompleteRequest> {

    final Filters filters;

    public PacketTabRequestListener(CommandBlocker plugin) {
        super(TabCompleteRequest.class, Direction.UPSTREAM, 0);
        this.filters = plugin.getFilters();
    }

    @Override
    public void packetReceive(PacketReceiveEvent<TabCompleteRequest> e) {
        TabCompleteRequest req = e.packet();

        if (filters.apply(new BungeeUser(e.player().handle()), SlashUtil.apply(req.getCursor())) != null) {
            e.cancelled(true);
        }

        TabMemory.MEMORY.put(e.player().uniqueId(), new TabMemory(req.getTransactionId(), req.getCursor()));
    }

    @Override
    public void packetSend(PacketSendEvent<TabCompleteRequest> packetSendEvent) {}

}
