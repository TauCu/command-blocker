package me.taucu.commandblocker;

import com.google.common.io.ByteStreams;
import me.taucu.commandblocker.filters.AbstractFilter;
import me.taucu.commandblocker.filters.Filters;
import me.taucu.commandblocker.util.ConfigurationException;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConfigLoader {

    protected final File baseFile;
    protected final File configFile;
    protected final Logger log;

    protected final Filters filters;

    public ConfigLoader(File baseFile, String name, Logger log, Filters filters) {
        this.baseFile = baseFile;
        this.configFile = new File(baseFile, name);
        this.log = log;
        this.filters = filters;
    }

    /**
     * Used to reload the plugins configuration
     */
    public void reloadConfig() {
        baseFile.mkdirs();
        Config internalConfig = loadInternalConfig();
        Config config = loadConfig(internalConfig);
        Config filtersSection = config.getSection("filters");

        if (filtersSection == null) {
            throw new ConfigurationException("\"filters\" is an invalid configuration section");
        }

        filters.setDefaultPatternPrefix(config.getString("default pattern prefix"));
        filters.setDefaultPatternSuffix(config.getString("default pattern suffix"));
        filters.setDefaultType(config.getString("default type"));
        filters.defaultDenyMsg(config.getString("default deny message"));

        Config defaultActionsSection = config.getSection("default actions");
        if (defaultActionsSection == null) {
            throw new ConfigurationException("\"default actions\" is an invalid configuration section");
        } else {
            loadDefaultFilterActions(defaultActionsSection);
        }

        filters.setFilterPerm(config.getString("root filter permission"));
        filters.setDefaultPermissionCacheMillis(config.getInt("permission cache millis"));
        loadFilters(filtersSection);
    }

    public Config loadConfig(Config defaults) throws ConfigurationException {
        if (!configFile.isFile()) {
            try {
                saveDefaultConfig();
            } catch (Exception e) {
                throw new ConfigurationException("unable to save default config", e);
            }
        }
        Config config;
        try {
            try {
                config = YamlConfig.load(configFile);
                config.setDefaults(defaults);
            } catch (Exception e) {
                log.log(Level.SEVERE, "Exception while loading config, attempting to regenerate it.", e);
                regenerateConfig();
                config = YamlConfig.load(configFile);
                config.setDefaults(defaults);
            }

            double defaultVer = defaults.getDouble("config version");
            double fileVer = config.getDouble("config version");
            if (defaultVer != fileVer) {
                if ((int) defaultVer != (int) fileVer) {
                    log.warning("outdated configuration major version, regenerating.");
                    regenerateConfig();
                    config = YamlConfig.load(configFile);
                    config.setDefaults(defaults);
                } else {
                    log.warning("outdated configuration minor version. it is recommended to regenerate the config.");
                }
            }
            return config;
        } catch (Exception e) {
            throw new ConfigurationException("unable to load config", e);
        }
    }

    public void loadDefaultFilterActions(Config defaultActionsSection) {
        try {
            for (String k : defaultActionsSection.getKeys()) {
                String action;
                action = defaultActionsSection.getSection(k).getString("allow");
                if (action == null) {
                    throw new ConfigurationException("\"default actions\" is an invalid configuration section");
                } else {
                    filters.setDefaultAllowActionFor(k, action);
                }
                action = defaultActionsSection.getSection(k).getString("deny");
                if (action == null) {
                    throw new ConfigurationException("\"default actions\" is an invalid configuration section");
                } else {
                    filters.setDefaultDenyActionFor(k, action);
                }
            }
        } catch (Exception e) {
            throw new ConfigurationException("Error while loading a types default actions", e);
        }
    }

    public boolean loadFilters(Config rt) {
        log.info("loading filters...");
        boolean errorless = true;
        List<String> keys = new ArrayList<>(rt.getKeys());
        Comparator<String> com = (k1, k2) -> {
            Config c1 = rt.getSection(k1);
            Config c2 = rt.getSection(k2);
            if (c1 != null && c2 != null) {
                int p1, p2;
                if (c1.contains("priority")) {
                    p1 = c1.getInt("priority");
                } else {
                    p1 = 0;
                }
                if (c2.contains("priority")) {
                    p2 = c2.getInt("priority");
                } else {
                    p2 = 0;
                }
                return p2 - p1;
            }
            return 0;
        };
        keys.sort(com);

        List<AbstractFilter> filterList = new ArrayList<>();
        int patternCount = 0;
        for (String k : keys) {
            try {
                Config c = rt.getSection(k);
                if (c == null) {
                    throw new ConfigurationException(k + " is not a valid configuration section");
                }
                List<String> patterns = c.getStringList("patterns");
                if (patterns == null) {
                    throw new ConfigurationException("\"" + k + "." + "patterns\" is not a valid string list");
                }
                final String type = c.getString("type", filters.getDefaultType());
                AbstractFilter filter = filters.filterOfType(type, k.toLowerCase());
                filter.setPattern(patterns.toArray(new String[patterns.size() - 1]));
                if (c.contains("pattern prefix")) {
                    filter.setPatternPrefix(c.getString("pattern prefix"));
                }
                if (c.contains("pattern suffix")) {
                    filter.setPatternPrefix(c.getString("pattern suffix"));
                }
                if (c.contains("message")) {
                    filter.setDenyMsg(c.getString("message"));
                }
                if (c.contains("allow action")) {
                    filter.setAllowAction(filters.actionByName(c.getString("allow action")));
                } else {
                    filter.setAllowAction(filters.getDefaultAllowActionFor(type));
                }
                if (c.contains("deny action")) {
                    filter.setDenyAction(filters.actionByName(c.getString("deny action")));
                } else {
                    filter.setDenyAction(filters.getDefaultDenyActionFor(type));
                }
                filterList.add(filter);
                patternCount += patterns.size();
            } catch (Exception e) {
                log.log(Level.SEVERE, "could not load filter \"" + k + "\" with error: ", e);
                errorless = false;
            }
        }
        filters.setFilters(filterList);

        log.info("loaded " + filterList.size() + " filters with " + patternCount + " pattern(s)");
        return errorless;
    }

    public void regenerateConfig() {
        try {
            for (int i = 0; i < 100; i++) {
                File to = new File(baseFile, String.format("config.old.%s.yml", i));
                if (!to.exists()) {
                    Files.move(configFile.toPath(), to.toPath());
                    saveDefaultConfig();
                    return;
                }
            }
            throw new ConfigurationException("unable to move config file, there are too many old configs");
        } catch (Exception e) {
            throw new ConfigurationException("error while regenerating old config file", e);
        }
    }

    public InputStream streamInternalConfig() {
        return this.getClass().getClassLoader().getResourceAsStream("config.yml");
    }

    public YamlConfig loadInternalConfig() {
        try (InputStream in = streamInternalConfig()) {
            return YamlConfig.load(in);
        } catch (IOException e) {
            throw new ConfigurationException("could not load internal config", e);
        }
    }

    public void saveDefaultConfig() {
        try {
            baseFile.mkdirs();
            configFile.createNewFile();
            try (InputStream in = streamInternalConfig(); OutputStream os = new FileOutputStream(configFile)) {
                ByteStreams.copy(in, os);
            }
        } catch (IOException e) {
            throw new ConfigurationException("Error while writing default config", e);
        }
    }

}
