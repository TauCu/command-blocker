package me.taucu.commandblocker.platform.bungeecord.listeners;

import com.mojang.brigadier.suggestion.Suggestion;
import com.mojang.brigadier.suggestion.Suggestions;
import dev.simplix.protocolize.api.Direction;
import dev.simplix.protocolize.api.listener.PacketReceiveEvent;
import dev.simplix.protocolize.api.listener.PacketSendEvent;
import me.taucu.commandblocker.CommandBlocker;
import me.taucu.commandblocker.User;
import me.taucu.commandblocker.filters.Filters;
import me.taucu.commandblocker.platform.bungeecord.BungeeUser;
import me.taucu.commandblocker.util.TabMemory;
import me.taucu.commandblocker.util.packets.ProtocolizePacketListener;
import me.taucu.commandblocker.util.text.SlashUtil;
import net.md_5.bungee.protocol.packet.TabCompleteResponse;

import java.util.Iterator;

public class PacketTabResponseListener extends ProtocolizePacketListener<TabCompleteResponse> {

    final CommandBlocker pl;
    final Filters filters;

    public PacketTabResponseListener(CommandBlocker pl) {
        super(TabCompleteResponse.class, Direction.UPSTREAM, 0);
        this.pl = pl;
        this.filters = pl.getFilters();
    }

    @Override
    public void packetReceive(PacketReceiveEvent<TabCompleteResponse> packetReceiveEvent) {}

    @Override
    public void packetSend(PacketSendEvent<TabCompleteResponse> e) {
        TabCompleteResponse res = e.packet();
        TabMemory mem = TabMemory.MEMORY.get(e.player().uniqueId());
        if (mem != null) {
            User u = new BungeeUser(e.player().handle());
            if (res.getSuggestions() != null) {
                Suggestions s = res.getSuggestions();
                if (res.getTransactionId() == mem.transactionId && mem.request.length() >= s.getRange().getStart()) {
                    String requestCommand = mem.request.substring(0, s.getRange().getStart());
                    Iterator<Suggestion> it = s.getList().iterator();
                    while (it.hasNext()) {
                        if (filters.apply(u, SlashUtil.apply(requestCommand + it.next().getText())) != null) {
                            it.remove();
                        }
                    }

                    if (!s.getList().isEmpty()) {
                        return; //valid responses
                    }
                }
            } else { // old protocols
                Iterator<String> it = res.getCommands().iterator();
                while (it.hasNext()) {
                    if (filters.apply(u, it.next()) != null) {
                        it.remove();
                    }
                }

                if (!res.getCommands().isEmpty()) {
                    return; //valid responses
                }
            }
        }
        e.cancelled(true); //nonsense or empty responses
    }

}
