package me.taucu.commandblocker.filters;

public enum FilterAction {
    
    ALLOW, DENY, SOFT_ALLOW, SOFT_DENY, NONE;

}
