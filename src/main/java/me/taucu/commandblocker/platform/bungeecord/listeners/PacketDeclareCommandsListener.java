package me.taucu.commandblocker.platform.bungeecord.listeners;

import com.mojang.brigadier.tree.CommandNode;
import com.mojang.brigadier.tree.RootCommandNode;
import dev.simplix.protocolize.api.Direction;
import dev.simplix.protocolize.api.listener.PacketReceiveEvent;
import dev.simplix.protocolize.api.listener.PacketSendEvent;
import me.taucu.commandblocker.User;
import me.taucu.commandblocker.filters.Filters;
import me.taucu.commandblocker.platform.bungeecord.BungeeCommandBlocker;
import me.taucu.commandblocker.platform.bungeecord.BungeeUser;
import me.taucu.commandblocker.util.packets.ProtocolizePacketListener;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.protocol.packet.Commands;

import java.util.Iterator;

public class PacketDeclareCommandsListener extends ProtocolizePacketListener<Commands> {

    private final Filters filters;

    public PacketDeclareCommandsListener(BungeeCommandBlocker pl) {
        super(Commands.class, Direction.UPSTREAM, 0);
        this.filters = pl.getFilters();
    }

    @Override
    public void packetSend(PacketSendEvent<Commands> e) {
        RootCommandNode<?> root = e.packet().getRoot();
        Object sender = e.player().handle();
        if (root != null && sender instanceof ProxiedPlayer) {
            filterNodes(new BungeeUser((ProxiedPlayer) sender), root);
        }
    }

    @Override
    public void packetReceive(PacketReceiveEvent<Commands> paramPacketReceiveEvent) {}

    private void filterNodes(User u, CommandNode<?> node) {
        Iterator<? extends CommandNode<?>> it = node.getChildren().iterator();
        while (it.hasNext()) {
            node = it.next();
            if (filters.apply(u, '/' + node.getName()) != null) {
                it.remove();
            }
        }
    }

}

