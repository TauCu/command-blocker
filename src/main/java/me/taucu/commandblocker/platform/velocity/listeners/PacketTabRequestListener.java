package me.taucu.commandblocker.platform.velocity.listeners;

import com.velocitypowered.proxy.protocol.packet.TabCompleteRequestPacket;
import dev.simplix.protocolize.api.Direction;
import dev.simplix.protocolize.api.listener.PacketReceiveEvent;
import dev.simplix.protocolize.api.listener.PacketSendEvent;
import me.taucu.commandblocker.CommandBlocker;
import me.taucu.commandblocker.filters.Filters;
import me.taucu.commandblocker.platform.velocity.VelocityUser;
import me.taucu.commandblocker.util.TabMemory;
import me.taucu.commandblocker.util.packets.ProtocolizePacketListener;
import me.taucu.commandblocker.util.text.SlashUtil;

public class PacketTabRequestListener extends ProtocolizePacketListener<TabCompleteRequestPacket> {

    final Filters filters;

    public PacketTabRequestListener(CommandBlocker plugin) {
        super(TabCompleteRequestPacket.class, Direction.UPSTREAM, 0);
        this.filters = plugin.getFilters();
    }

    @Override
    public void packetReceive(PacketReceiveEvent<TabCompleteRequestPacket> e) {
        TabCompleteRequestPacket req = e.packet();

        if (filters.apply(new VelocityUser(e.player().handle()), SlashUtil.apply(req.getCommand())) != null) {
            e.cancelled(true);
        }

        // always redefine to ensure an atomic, consistent state
        TabMemory.MEMORY.put(e.player().uniqueId(), new TabMemory(req.getTransactionId(), req.getCommand()));
    }

    @Override
    public void packetSend(PacketSendEvent<TabCompleteRequestPacket> packetSendEvent) {}

}
