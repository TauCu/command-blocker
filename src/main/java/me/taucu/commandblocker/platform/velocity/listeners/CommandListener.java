package me.taucu.commandblocker.platform.velocity.listeners;

import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.command.CommandExecuteEvent;
import com.velocitypowered.api.proxy.Player;
import me.taucu.commandblocker.CommandBlocker;
import me.taucu.commandblocker.User;
import me.taucu.commandblocker.filters.AbstractFilter;
import me.taucu.commandblocker.filters.Filters;
import me.taucu.commandblocker.platform.velocity.VelocityUser;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;

public class CommandListener {

    final Filters filters;

    final LegacyComponentSerializer serializer = LegacyComponentSerializer.legacyAmpersand();

    public CommandListener(CommandBlocker pl) {
        this.filters = pl.getFilters();
    }

    @Subscribe
    public void onCommand(CommandExecuteEvent e) {
        if (e.getCommandSource() instanceof Player) {
            User u = new VelocityUser((Player) e.getCommandSource());
            AbstractFilter filter = filters.apply(u, '/' + e.getCommand());
            if (filter != null) {
                e.setResult(CommandExecuteEvent.CommandResult.denied());
                if (filter.shouldSendDenyMessage()) {
                    e.getCommandSource().sendMessage(serializer.deserialize(filter.getDenyMsg().replace("%command%", '/' + e.getCommand())));
                }
            }
        }
    }

}
