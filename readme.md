# Command Blocker

Command blocker is a plugin that prevents users from using or even seeing specific commands.

It supports Bukkit, BungeeCord and Velocity.

Unlike other command blocking plugins, this one supports regex as well as a wide variety of different filters & settings
giving you fine control over what, how and where commands are blocked.

---

### Installation
1. Stop server (if running)
2. Add plugin to `./plugins` folder
3. Start server to generate default config in `./plugins/CommandBlocker/config.yml`
4. Configure to liking and run the plugins' reload command

### Commands
- `/commandblocker` Bukkit
- `/bcommandblocker` Bungee
- `/vcommandblocker` Velocity
  - `reload`
  - `version`
  - `sendCommands <player>` (bukkit only)
      - filters and resends all commands to a specific player
  - `syncCommands` (bukkit only)
    - similar to __sendCommands__ but effects all players and is more intensive
    - causes all commands to be recalculated in the event that a command was added or removed from the CommandMap

### Permissions
- `tau.cmdblock.command` 
  - Grants access to plugins own commands
- `tau.cmdblock.filter.<filter>` 
  - used to either apply or bypass a filter depending on its mode

### Dependencies
Command Blocker requires [Protocolize](https://www.spigotmc.org/resources/protocolize-protocollib-for-bungeecord-waterfall-velocity.63778/) when running on BungeeCord or Velocity and [ProtocolLib](https://www.spigotmc.org/resources/protocollib.1997/) when running on Bukkit

### Other
If required, you can change what command aliases the plugin uses in the [command-blocker.properties](src/main/resources/command-blocker.properties) file within the jar