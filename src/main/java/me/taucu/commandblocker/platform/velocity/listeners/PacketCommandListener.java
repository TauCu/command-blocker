package me.taucu.commandblocker.platform.velocity.listeners;

import com.velocitypowered.api.proxy.Player;
import com.velocitypowered.proxy.protocol.packet.chat.session.SessionPlayerCommandPacket;
import dev.simplix.protocolize.api.Direction;
import dev.simplix.protocolize.api.listener.PacketReceiveEvent;
import dev.simplix.protocolize.api.listener.PacketSendEvent;
import me.taucu.commandblocker.CommandBlocker;
import me.taucu.commandblocker.User;
import me.taucu.commandblocker.filters.AbstractFilter;
import me.taucu.commandblocker.filters.Filters;
import me.taucu.commandblocker.platform.velocity.VelocityUser;
import me.taucu.commandblocker.util.packets.ProtocolizePacketListener;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;

public class PacketCommandListener extends ProtocolizePacketListener<SessionPlayerCommandPacket> {

    final Filters filters;

    final LegacyComponentSerializer serializer = LegacyComponentSerializer.legacyAmpersand();

    public PacketCommandListener(CommandBlocker pl) {
        super(SessionPlayerCommandPacket.class, Direction.UPSTREAM, 0);
        this.filters = pl.getFilters();
    }

    @Override
    public void packetReceive(PacketReceiveEvent<SessionPlayerCommandPacket> e) {
        SessionPlayerCommandPacket chat = e.packet();
        User u = new VelocityUser(e.player().handle());
        AbstractFilter filter = filters.apply(u, '/' + chat.getCommand());
        if (filter != null) {
            e.cancelled(true);
            if (filter.shouldSendDenyMessage()) {
                ((Player) e.player().handle()).sendMessage(serializer.deserialize(filter.getDenyMsg().replace("%command%", '/' + chat.getCommand())));
            }
        }
    }

    @Override
    public void packetSend(PacketSendEvent<SessionPlayerCommandPacket> packetSendEvent) {}

}
