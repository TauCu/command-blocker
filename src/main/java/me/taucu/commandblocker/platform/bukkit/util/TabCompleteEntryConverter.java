package me.taucu.commandblocker.platform.bukkit.util;

import com.comphenix.protocol.reflect.EquivalentConverter;

public class TabCompleteEntryConverter implements EquivalentConverter<WrappedTabCompleteEntry> {

    public static final TabCompleteEntryConverter INSTANCE = new TabCompleteEntryConverter();

    private TabCompleteEntryConverter() {}

    @Override
    public Object getGeneric(WrappedTabCompleteEntry specific) {
        return specific.getHandle();
    }

    @Override
    public WrappedTabCompleteEntry getSpecific(Object generic) {
        return new WrappedTabCompleteEntry(generic);
    }

    @Override
    public Class<WrappedTabCompleteEntry> getSpecificType() {
        return WrappedTabCompleteEntry.class;
    }

}
