package me.taucu.commandblocker.filters;

import me.taucu.commandblocker.User;
import me.taucu.commandblocker.filters.types.Blacklist;
import me.taucu.commandblocker.filters.types.RestrictedBlacklist;
import me.taucu.commandblocker.filters.types.RestrictedWhitelist;
import me.taucu.commandblocker.filters.types.Whitelist;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

public class Filters {
    
    private List<AbstractFilter> filters = new CopyOnWriteArrayList<AbstractFilter>();
    
    private String defaultPatternPrefix = "(?i)^\\/";
    private String defaultPatternSuffix = "(\\s|$)";
    private String defaultType = "blacklist";
    private String defaultDenyMsg = "I'm sorry, but you do not have permission to perform this command. Please contact the server administrators if you believe that this is in error.";
    private String rootFilterPerm = "tau.cmdblock.filter";
    
    private Map<String, FilterAction> actionAllowMap = Collections.synchronizedMap(new HashMap<String, FilterAction>());
    private Map<String, FilterAction> actionDenyMap = Collections.synchronizedMap(new HashMap<String, FilterAction>());
    
    private long permissionCacheMillis = 100;
    
    public Filters() {
    }
    
    public AbstractFilter apply(User u, String c) {
        //System.out.println("apply from:" + Thread.currentThread().getStackTrace()[2].getClassName() + "#" + Thread.currentThread().getStackTrace()[2].getMethodName());
        //System.out.println("  applying:" + c);
        AbstractFilter softDeny = null;
        for (AbstractFilter filter : filters) {
            switch (filter.apply(u, c)) {
                case ALLOW:
                    return null;
                case DENY:
                    return filter;
                case SOFT_ALLOW:
                    softDeny = null;
                    break;
                case SOFT_DENY:
                    softDeny = filter;
                    break;
                default:
                    break;
            }
        }
        return softDeny;
    }
    
    public String getDefaultPatternPrefix() {
        return defaultPatternPrefix;
    }
    
    public void setDefaultPatternPrefix(String defaultRegexPrefix) {
        this.defaultPatternPrefix = defaultRegexPrefix;
    }
    
    public String getDefaultPatternSuffix() {
        return defaultPatternSuffix;
    }
    
    public void setDefaultPatternSuffix(String defaultPatternSuffix) {
        this.defaultPatternSuffix = defaultPatternSuffix;
    }
    
    public String getDefaultType() {
        return defaultType;
    }
    
    public void setDefaultType(String defaultType) {
        this.defaultType = defaultType;
    }
    
    public FilterAction getDefaultAllowActionFor(String type) {
        FilterAction val = actionAllowMap.get(type.toLowerCase());
        if (val == null) {
            throw new NoSuchElementException("No default allow action found for \"" + type + "\"");
        } else {
            return val;
        }
    }
    
    public FilterAction getDefaultDenyActionFor(String type) {
        FilterAction val = actionDenyMap.get(type.toLowerCase());
        if (val == null) {
            throw new NoSuchElementException("No default deny action found for \"" + type + "\"");
        } else {
            return val;
        }
    }
    
    public void setDefaultAllowActionFor(String type, String action) {
        type = type.toLowerCase();
        action = action.toUpperCase();
        
        actionAllowMap.put(type, actionByName(action));
    }
    
    public void setDefaultDenyActionFor(String type, String action) {
        type = type.toLowerCase();
        action = action.toUpperCase();
        
        actionDenyMap.put(type, actionByName(action));
    }
    
    public String defaultDenyMsg() {
        return defaultDenyMsg;
    }
    
    public void defaultDenyMsg(String m) {
        if (m == null) m = "";
        this.defaultDenyMsg = m;
    }
    
    public String getRootFilterPerm() {
        return rootFilterPerm;
    }
    
    public void setFilterPerm(String rootFilterPerm) {
        this.rootFilterPerm = rootFilterPerm;
    }
    
    public void setDefaultPermissionCacheMillis(long millis) {
        this.permissionCacheMillis = millis;
    }
    
    public List<AbstractFilter> getFilters() {
        return filters;
    }
    
    public void add(AbstractFilter f) {
        if (!filters.contains(f)) {
            filters.add(f);
        }
    }
    
    public void remove(AbstractFilter f) {
        filters.remove(f);
    }

    public void setFilters(List<AbstractFilter> filters) {
        this.filters = new CopyOnWriteArrayList<>(filters);
    }
    
    public void clear() {
        filters.clear();
    }
    
    public AbstractFilter filterOfType(String type, String name) {
        if (type == null || type.isEmpty()) {
            type = defaultType;
        }
        switch (type.toLowerCase()) {
        case "whitelist":
            return new Whitelist(this, name);
        case "blacklist":
            return new Blacklist(this, name);
        case "restrictedwhitelist":
            return new RestrictedWhitelist(this, name);
        case "restrictedblacklist":
            return new RestrictedBlacklist(this, name);
        default:
            throw new NoSuchElementException("no filter found by type \"" + type + "\"");
        }
    }
    
    public FilterAction actionByName(String name) {
        if (name != null) {
            try {
                return FilterAction.valueOf(name.toUpperCase());
            } catch (IllegalArgumentException e) {
                throw new NoSuchElementException("no filter action found by the name of \"" + name + "\"");
            }
        } else {
            throw new NullPointerException("cannot get action by a null name \"" + name + "\"");
        }
    }
    
    public long getPermissionCacheMillis() {
        return permissionCacheMillis;
    }
    
}
