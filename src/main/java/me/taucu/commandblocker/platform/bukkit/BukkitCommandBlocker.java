package me.taucu.commandblocker.platform.bukkit;

import me.taucu.commandblocker.CommandBlocker;
import me.taucu.commandblocker.ConfigLoader;
import me.taucu.commandblocker.filters.Filters;
import me.taucu.commandblocker.platform.bukkit.commands.BukkitCommand;
import me.taucu.commandblocker.platform.bukkit.listeners.*;
import me.taucu.commandblocker.platform.bukkit.util.CommandUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandMap;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.Iterator;
import java.util.Properties;
import java.util.logging.Level;

public class BukkitCommandBlocker extends JavaPlugin implements CommandBlocker {

    private final Properties pluginProperties = loadInternalProperties("command-blocker.properties", new Properties());
    private final Filters filters = new Filters();
    private final ConfigLoader configLoader = new ConfigLoader(getDataFolder(), "config.yml", getLogger(), filters);

    private BukkitCommand primaryCommand = null;

    private PacketTabRequestListener packetTabRequestListener = null;
    private PacketTabResponseListener packetTabResponseListener = null;

    @Override
    public void onEnable() {
        registerCommands();
        configLoader.reloadConfig();
        registerEvents(new CommandListener(this));
        registerEvents(new TabCompleteListener(this));
        registerEvents(new DeclareCommandsListener(this));
        if (Bukkit.getPluginManager().isPluginEnabled("ProtocolLib")) {
            try {
                this.packetTabRequestListener = new PacketTabRequestListener(this).register();
                this.packetTabResponseListener = new PacketTabResponseListener(this).register();
            } catch (Exception ex) {
                getLogger().log(Level.SEVERE, "Error while enabling ProtocolLib packet listeners!", ex);
            }
        } else {
            getLogger().warning("ProtocolLib is not enabled! ProtocolLib is required to fix an exploit allowing players to query server commands.");
        }
    }

    @Override
    public void onDisable() {
        if (packetTabRequestListener != null) {
            packetTabRequestListener.unregister();
        }

        if (packetTabResponseListener != null) {
            packetTabResponseListener.unregister();
        }

        // support plugin loaders.
        // many of them can't seem to figure out how the command map works (thus creating a memory leak) so let's help them...

        // this doesn't work. why?
        try {
            CommandMap map = CommandUtil.getCommandMap();
            primaryCommand.unregister(map);

            // this, however, does work.
            Iterator<Command> it = CommandUtil.getKnownCommands(map).values().iterator();
            while (it.hasNext()) {
                if (it.next() == primaryCommand) {
                    it.remove();
                }
            }
        } catch (ReflectiveOperationException e) {
            getLogger().log(Level.SEVERE, "Failed to unregister commands", e);
        }
        primaryCommand.syncCommands();
    }

    @Override
    public File getDataDirectory() {
        return getDataFolder();
    }

    @Override
    public ConfigLoader getConfigLoader() {
        return configLoader;
    }

    @Override
    public Filters getFilters() {
        return filters;
    }

    @Override
    public String getPluginVersion() {
        return getDescription().getVersion();
    }

    @Override
    public Properties getPluginProperties() {
        return pluginProperties;
    }

    public void registerCommands() {
        primaryCommand = new BukkitCommand(this, parseAliases("bukkit-command-aliases", "cmdblocker cmdblock cb"));
        try {
            CommandMap map = CommandUtil.getCommandMap();
            map.register("commandblocker", primaryCommand);
        } catch (ReflectiveOperationException e) {
            getLogger().log(Level.SEVERE, "Failed to register commands", e);
        }
    }

    public <T extends Listener> T registerEvents(T l) {
        Bukkit.getPluginManager().registerEvents(l, this);
        return l;
    }

}
