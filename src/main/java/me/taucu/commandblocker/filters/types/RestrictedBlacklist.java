package me.taucu.commandblocker.filters.types;

import me.taucu.commandblocker.User;
import me.taucu.commandblocker.filters.AbstractFilter;
import me.taucu.commandblocker.filters.FilterAction;
import me.taucu.commandblocker.filters.Filters;

public class RestrictedBlacklist extends AbstractFilter {
    
    public RestrictedBlacklist(Filters parent, String name) {
        super(parent, name);
    }
    
    @Override
    public FilterAction apply(User u, String command) {
        if (this.checkPermission(u) && this.getMatcher(command).find()) {
            return denyAction;
        } else {
            return allowAction;
        }
    }
    
}
