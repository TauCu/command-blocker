package me.taucu.commandblocker.platform.velocity;

import com.google.inject.Inject;
import com.velocitypowered.api.command.BrigadierCommand;
import com.velocitypowered.api.command.CommandMeta;
import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.proxy.ProxyInitializeEvent;
import com.velocitypowered.api.event.proxy.ProxyShutdownEvent;
import com.velocitypowered.api.plugin.annotation.DataDirectory;
import com.velocitypowered.api.proxy.ProxyServer;
import me.taucu.commandblocker.platform.AbstractCommandBlocker;
import me.taucu.commandblocker.platform.velocity.commands.VelocityCommand;
import me.taucu.commandblocker.platform.velocity.listeners.*;
import me.taucu.commandblocker.util.TabMemory;

import java.nio.file.Path;
import java.util.logging.Logger;

public class VelocityCommandBlocker extends AbstractCommandBlocker {

    private final ProxyServer server;

    private final VelocityCommand primaryCommand = new VelocityCommand(this);

    private PacketTabRequestListener packetTabRequestListener = null;
    private PacketTabResponseListener packetTabResponseListener = null;
    private PacketCommandListener packetCommandListener = null;

    @Inject
    public VelocityCommandBlocker(ProxyServer server, @DataDirectory Path dataDirectory) {
        super(Logger.getLogger("commandblocker"), dataDirectory.toFile(), "config.yml");
        this.server = server;
    }

    @Subscribe
    public void onInit(ProxyInitializeEvent event) {
        registerCommands();
        configLoader.reloadConfig();
        registerEvents(new CommandListener(this));
        registerEvents(new DeclareCommandsListener(this));
        registerEvents(new TabCompleteListener(this));
        if (server.getPluginManager().isLoaded("protocolize")) {
            TabMemory.MEMORY.clear();
            this.packetCommandListener = new PacketCommandListener(this).register();
            registerEvents(new PlayerListener()); // used to clean up TabMemory
            this.packetTabRequestListener = new PacketTabRequestListener(this).register();
            this.packetTabResponseListener = new PacketTabResponseListener(this).register();
        } else {
            getLogger().severe("Protocolize not found; Protocolize is required for this plugin to work on velocity.");
        }
    }

    @Subscribe
    public void onShutdown(ProxyShutdownEvent event) {
        if (packetTabRequestListener != null) {
            packetTabRequestListener.unregister();
        }
        if (packetTabResponseListener != null) {
            packetTabResponseListener.unregister();
        }
        if (packetCommandListener != null) {
            packetCommandListener.unregister();
        }
    }
    
    public void registerCommands() {
        BrigadierCommand command = new BrigadierCommand(primaryCommand.root);
        CommandMeta meta = server.getCommandManager()
                .metaBuilder(command)
                .aliases(parseAliases("velocity-command-aliases", "vcmdblocker vcmdblock vcb"))
                .build();
        server.getCommandManager().register(meta, command);
    }
    
    public <T> T registerEvents(T l) {
        server.getEventManager().register(this, l);
        return l;
    }

    public ProxyServer getServer() {
        return server;
    }

}
