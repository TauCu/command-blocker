package me.taucu.commandblocker.platform.bukkit.listeners;

import me.taucu.commandblocker.User;
import me.taucu.commandblocker.filters.Filters;
import me.taucu.commandblocker.platform.bukkit.BukkitCommandBlocker;
import me.taucu.commandblocker.platform.bukkit.BukkitUser;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandSendEvent;

import java.util.Iterator;

public class DeclareCommandsListener implements Listener {
    
    private final Filters filters;
    
    public DeclareCommandsListener(BukkitCommandBlocker pl) {
        this.filters = pl.getFilters();
    }
    
    @EventHandler(priority = EventPriority.LOW)
    public void onCommandSend(PlayerCommandSendEvent e) {
        Iterator<String> it = e.getCommands().iterator();
        User u = new BukkitUser(e.getPlayer());
        while (it.hasNext()) {
            if (filters.apply(u, '/' + it.next()) != null) {
                it.remove();
            }
        }
    }
    
}
