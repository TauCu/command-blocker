package me.taucu.commandblocker.platform.bukkit.listeners;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.utility.MinecraftProtocolVersion;
import com.mojang.brigadier.suggestion.Suggestion;
import com.mojang.brigadier.suggestion.Suggestions;
import me.taucu.commandblocker.User;
import me.taucu.commandblocker.filters.Filters;
import me.taucu.commandblocker.platform.bukkit.BukkitCommandBlocker;
import me.taucu.commandblocker.platform.bukkit.BukkitUser;
import me.taucu.commandblocker.platform.bukkit.util.TabCompleteEntryConverter;
import me.taucu.commandblocker.platform.bukkit.util.WrappedTabCompleteEntry;
import me.taucu.commandblocker.util.TabMemory;
import me.taucu.commandblocker.util.text.SlashUtil;

import java.util.Iterator;
import java.util.List;

public class PacketTabResponseListener extends PacketAdapter {

    final Filters filters;

    public PacketTabResponseListener(BukkitCommandBlocker plugin) {
        super(PacketAdapter.params(plugin, PacketType.Play.Server.TAB_COMPLETE).optionAsync());
        filters = plugin.getFilters();
    }

    @Override
    public void onPacketSending(PacketEvent e) {
        if (e.isCancelled()) return;

        PacketContainer packet = e.getPacket();
        TabMemory mem = TabMemory.MEMORY.get(e.getPlayer().getUniqueId());

        if (mem != null) {
            int transactionId = packet.getIntegers().read(0);
            // tab complete was rewritten from 1.20.6 onwards
            if (MinecraftProtocolVersion.getCurrentVersion() > 765) {
                int start = packet.getIntegers().read(1);
                List<WrappedTabCompleteEntry> entries = packet.getLists(TabCompleteEntryConverter.INSTANCE).read(0);

                if (transactionId == mem.transactionId && mem.request.length() >= start) {
                    User u = new BukkitUser(e.getPlayer());
                    String requestCommand = mem.request.substring(0, start);
                    Iterator<WrappedTabCompleteEntry> it = entries.iterator();
                    while (it.hasNext()) {
                        // As of writing this VIA breaks double slash commands... oh well. Not my job.
                        if (filters.apply(u, SlashUtil.apply(requestCommand + it.next().getText())) != null) {
                            it.remove();
                        }
                    }
                }

                if (entries.isEmpty()) {
                    // nonsense or empty responses
                    e.setCancelled(true);
                }
            } else {
                Suggestions suggestions = packet.getSpecificModifier(Suggestions.class).read(0);

                boolean change = false;
                if (transactionId == mem.transactionId && mem.request.length() >= suggestions.getRange().getStart()) {
                    User u = new BukkitUser(e.getPlayer());
                    String requestCommand = mem.request.substring(0, suggestions.getRange().getStart());
                    Iterator<Suggestion> it = suggestions.getList().iterator();
                    while (it.hasNext()) {
                        // As of writing this VIA breaks double slash commands... oh well. Not my job.
                        if (filters.apply(u, SlashUtil.apply(requestCommand + it.next().getText())) != null) {
                            it.remove();
                            change = true;
                        }
                    }
                }

                if (suggestions.getList().isEmpty()) {
                    // nonsense or empty responses
                    e.setCancelled(true);
                } else if (change) {
                    // valid responses
                    packet.getSpecificModifier(Suggestions.class).write(0, suggestions);
                }
            }
        }

    }

    public PacketTabResponseListener register() {
        ProtocolLibrary.getProtocolManager().addPacketListener(this);
        return this;
    }

    public PacketTabResponseListener unregister() {
        ProtocolLibrary.getProtocolManager().removePacketListener(this);
        return this;
    }

}
