package me.taucu.commandblocker.platform.bungeecord.listeners;

import me.taucu.commandblocker.User;
import me.taucu.commandblocker.filters.AbstractFilter;
import me.taucu.commandblocker.filters.Filters;
import me.taucu.commandblocker.platform.bungeecord.BungeeCommandBlocker;
import me.taucu.commandblocker.platform.bungeecord.BungeeUser;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class CommandListener implements Listener {
    
    private final Filters filters;
    
    public CommandListener(BungeeCommandBlocker pl) {
        this.filters = pl.getFilters();
    }
    
    @EventHandler
    @SuppressWarnings("deprecation")
    public void onChat(ChatEvent e) {
        if (e.isCommand() && e.getSender() instanceof ProxiedPlayer) {
            User u = new BungeeUser((ProxiedPlayer) e.getSender());
            AbstractFilter filter = filters.apply(u, e.getMessage());
            if (filter != null) {
                e.setCancelled(true);
                if (filter.shouldSendDenyMessage()) {
                    ((ProxiedPlayer) e.getSender()).sendMessage(ChatColor.translateAlternateColorCodes('&', filter.getDenyMsg().replace("%command%", e.getMessage())));
                }
            }
        }
    }
    
}
