package me.taucu.commandblocker;

import me.taucu.commandblocker.util.ConfigurationException;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.LoaderOptions;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;
import org.yaml.snakeyaml.representer.Representer;

import java.io.*;
import java.util.*;

public class YamlConfig implements Config {

    private static final ThreadLocal<Yaml> yaml = ThreadLocal.withInitial(() -> {
        DumperOptions dumperOptions = new DumperOptions();
        LoaderOptions loaderOptions = new LoaderOptions();
        Representer representer = new Representer(dumperOptions) {
            {
                this.representers.put(YamlConfig.class, data -> represent(((YamlConfig) data).map));
            }
        };
        DumperOptions options = new DumperOptions();
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        return new Yaml(new Constructor(loaderOptions), representer, options);
    });

    public static Yaml getThreadLocalYaml() {
        return yaml.get();
    }

    @SuppressWarnings("unchecked")
    public static YamlConfig load(InputStream in) {
        Object val = yaml.get().loadAs(in, LinkedHashMap.class);
        if (val == null) {
            throw new ConfigurationException("Failed to load config: map object is null");
        }
        return new YamlConfig((Map<String, Object>) val);
    }

    @SuppressWarnings("unchecked")
    public static YamlConfig load(String config) {
        Object val = yaml.get().loadAs(config, LinkedHashMap.class);
        if (val == null) {
            throw new ConfigurationException("Failed to load config: map object is null");
        }
        return new YamlConfig((Map<String, Object>) val);
    }

    public static Config load(File configFile) {
        try (InputStream in = new FileInputStream(configFile)) {
            return load(in);
        } catch (IOException e) {
            throw new ConfigurationException("IOException while loading config", e);
        }
    }

    public static void write(YamlConfig config, OutputStream os) {
        yaml.get().dump(config, new OutputStreamWriter(os));
    }

    public static String writeAsString(YamlConfig config) {
        return yaml.get().dump(config);
    }

    public final Map<String, Object> map;
    public Config defaults = null;

    public YamlConfig(Map<String, Object> map) {
        this.map = map;
    }

    @Override
    public void setDefaults(Config defaults) {
        this.defaults = defaults;
    }

    @Override
    public Object get(String key) {
        return map.getOrDefault(key, defaults == null ? null : defaults.get(key));
    }

    @Override
    @SuppressWarnings("unchecked")
    public Config getSection(String path) {
        Object val = get(path);
        if (val instanceof Map) {
            return new YamlConfig((Map<String, Object>) val);
        }
        return new YamlConfig(new LinkedHashMap<>());
    }

    @Override
    public Collection<String> getKeys() {
        return new ArrayList<String>(map.keySet());
    }

    @Override
    public String getString(String key) {
        Object val = get(key);
        return val instanceof String ? (String) val : "";
    }

    @Override
    public String getString(String key, String defaultValue) {
        Object val = map.get(key);
        return val instanceof String ? (String) val : defaultValue;
    }

    @Override
    public int getInt(String key) {
        Object val = get(key);
        return val instanceof Number ? ((Number) val).intValue() : 0;
    }

    @Override
    public double getDouble(String key) {
        Object val = get(key);
        return val instanceof Number ? ((Number) val).doubleValue() : 0D;
    }

    @Override
    public List<String> getStringList(String key) {
        Object val = get(key);
        if (val instanceof List) {
            List<?> origin = (List<?>) val;
            List<String> results = new ArrayList<String>(origin.size());

            for (Object o : (List<?>) val) {
                if (o instanceof String) {
                    results.add((String) o);
                }
            }

            return results;
        }
        return new ArrayList<String>();
    }

    @Override
    public boolean contains(String key) {
        return map.containsKey(key);
    }

}
