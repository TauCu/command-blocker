package me.taucu.commandblocker.platform.bukkit.util;

import me.taucu.commandblocker.util.ReflectUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandMap;

import java.lang.reflect.Field;
import java.util.Map;

public abstract class CommandUtil {

    public static CommandMap getCommandMap() throws ReflectiveOperationException {
        try {
            Bukkit.class.getDeclaredMethod("getCommandMap");
            return Bukkit.getCommandMap();
        } catch (NoSuchMethodException e) {
            try {
                Field f = ReflectUtil.fuzzyFindDeclaredFieldByType(Bukkit.getServer().getClass(), CommandMap.class);
                f.setAccessible(true);
                return (CommandMap) f.get(Bukkit.getServer());
            } catch (Exception e2) {
                throw new ReflectiveOperationException("Failed to find CommandMap", e2);
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static Map<String, Command> getKnownCommands(CommandMap map) throws ReflectiveOperationException {
        Field f = map.getClass().getSuperclass().getDeclaredField("knownCommands");
        f.setAccessible(true);
        return (Map<String, Command>) f.get(map);
    }

}
