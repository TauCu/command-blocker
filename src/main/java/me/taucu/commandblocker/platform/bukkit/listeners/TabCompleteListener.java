package me.taucu.commandblocker.platform.bukkit.listeners;

import me.taucu.commandblocker.filters.Filters;
import me.taucu.commandblocker.platform.bukkit.BukkitCommandBlocker;
import me.taucu.commandblocker.platform.bukkit.BukkitUser;
import me.taucu.commandblocker.util.text.SlashUtil;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.TabCompleteEvent;

import java.util.Iterator;
import java.util.List;

public class TabCompleteListener implements Listener {
    
    private final Filters filters;
    
    public TabCompleteListener(BukkitCommandBlocker pl) {
        this.filters = pl.getFilters();
    }

    @EventHandler
    public void onTab(TabCompleteEvent e) {
        BukkitUser u = new BukkitUser(e.getSender());
        String command = SlashUtil.apply(e.getBuffer());
        if (filters.apply(u, command) != null) {
            e.setCancelled(true);
        } else {
            List<String> completions = e.getCompletions();
            Iterator<String> it = completions.iterator();
            boolean change = false;
            while (it.hasNext()) {
                if (filters.apply(u, command + it.next()) != null) {
                    it.remove();
                    change = true;
                }
            }
            if (change) {
                e.setCompletions(completions);
            }
        }
    }
    
}
