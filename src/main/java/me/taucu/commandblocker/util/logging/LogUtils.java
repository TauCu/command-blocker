package me.taucu.commandblocker.util.logging;

import org.bukkit.command.CommandSender;

import java.util.function.Consumer;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

public class LogUtils {

    /**
     * Creates an {@link AutoCloseable} wrapper for a handler and attaches it to this {@link Logger}
     * <p>
     * The handler will use the {@link SimpleChatColorLogFormatter#INSTANCE} as its formatter
     * @param log the {@link Logger} to attach to
     * @param sender the {@link CommandSender} to forward logs to via {@link CommandSender#sendMessage(String)}
     * @return the {@link LoggerAttachment}
     */
    public static LoggerAttachment attachLogger(Logger log, CommandSender sender) {
        return attachToLogger(log, sender::sendMessage);
    }

    /**
     * Creates an {@link AutoCloseable} wrapper for a handler and attaches it to this {@link Logger}
     * @param log the {@link Logger} to attach to
     * @param formatter the {@link Formatter} to use
     * @param sender the {@link CommandSender} to forward logs to via {@link CommandSender#sendMessage(String)}
     * @return the {@link LoggerAttachment}
     */
    public static LoggerAttachment attachLogger(Logger log, Formatter formatter, CommandSender sender) {
        return attachToLogger(log, formatter, sender::sendMessage);
    }

    /**
     * Creates an {@link AutoCloseable} wrapper for a handler and attaches it to this {@link Logger}
     * <p>
     * The handler will use the {@link SimpleChatColorLogFormatter#INSTANCE} as its formatter
     * @param log the {@link Logger} to attach to
     * @param consumer the {@link Consumer} to forward logs to
     * @return the {@link LoggerAttachment}
     */
    public static LoggerAttachment attachToLogger(Logger log, Consumer<String> consumer) {
        return attachToLogger(log, SimpleChatColorLogFormatter.INSTANCE, consumer);
    }

    /**
     * Creates an {@link AutoCloseable} wrapper for a handler and attaches it to this {@link Logger}
     * @param log the {@link Logger} to attach to
     * @param formatter the {@link Formatter} to use
     * @param consumer the {@link Consumer} to forward logs to
     * @return the {@link LoggerAttachment}
     */
    public static LoggerAttachment attachToLogger(Logger log, Formatter formatter, Consumer<String> consumer) {
        return new LoggerAttachment(log, formatter, consumer);
    }

    public static class LoggerAttachment implements AutoCloseable {

        private final Logger log;
        private volatile boolean open = true;
        private Consumer<String> logConsumer;

        public LoggerAttachment(Logger log, Consumer<String> logConsumer) {
            this(log, null, logConsumer);
        }

        public LoggerAttachment(Logger log, Formatter formatter, Consumer<String> logConsumer) {
            this.log = log;
            this.logConsumer = logConsumer;
            internalHandler.setFormatter(formatter);
            log.addHandler(internalHandler);
        }

        private final Handler internalHandler = new Handler() {

            @Override
            public void publish(LogRecord record) {
                if (open) {
                    Formatter formatter = getFormatter();
                    if (null == formatter) {
                        logConsumer.accept(record.getMessage());
                    } else {
                        logConsumer.accept(getFormatter().format(record));
                    }
                } else {
                    throw new RuntimeException("handler is closed");
                }
            }

            @Override
            public void flush() {
                if (!open) {
                    throw new RuntimeException("handler is closed");
                }
            }

            @Override
            public void close() throws SecurityException {
                try {
                    flush();
                    log.removeHandler(this);
                } finally {
                    open = false;
                }
            }
        };

        /**
         * Detaches and closes the wrapped {@link Handler}
         */
        @Override
        public void close() {
            internalHandler.close();
        }

        /**
         * checks if this attachment is closed
         * @return true of the attachment is closed, false otherwise
         */
        public boolean isClosed() {
            return !open;
        }

    }
    
}
