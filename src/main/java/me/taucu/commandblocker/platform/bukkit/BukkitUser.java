package me.taucu.commandblocker.platform.bukkit;

import me.taucu.commandblocker.User;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class BukkitUser implements User {

    public final CommandSender sender;
    public final Object key;
    public final Object context;

    public BukkitUser(CommandSender sender) {
        this.sender = sender;
        if (sender instanceof Player) {
            Player player = ((Player) sender);
            this.key = player.getUniqueId();
            this.context = player.getWorld().getUID();
        } else {
            this.key = sender;
            this.context = null;
        }
    }

    @Override
    public boolean hasPermission(String permission) {
        return sender.hasPermission(permission);
    }

    @Override
    public Object getKey() {
        return key;
    }

    @Override
    public Object getContext() {
        return context;
    }

}
