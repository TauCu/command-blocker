package me.taucu.commandblocker.util.packets;

import dev.simplix.protocolize.api.Direction;
import dev.simplix.protocolize.api.Protocolize;
import dev.simplix.protocolize.api.listener.AbstractPacketListener;

public abstract class ProtocolizePacketListener<T> extends AbstractPacketListener<T> {

    protected ProtocolizePacketListener(Class<T> type, Direction direction, int priority) {
        super(type, direction, priority);
    }

    @SuppressWarnings("unchecked")
    public <L extends ProtocolizePacketListener<T>> L register() {
        Protocolize.listenerProvider().registerListener(this);
        return (L) this;
    }

    @SuppressWarnings("unchecked")
    public <L extends ProtocolizePacketListener<T>> L unregister() {
        try { // for some fucking reason protocolize throws an exception if the listener isn't registered... while providing no method to check if the listener is registered.
            Protocolize.listenerProvider().unregisterListener(this);
        } catch (IllegalArgumentException ignored) {}
        return (L) this;
    }

}
