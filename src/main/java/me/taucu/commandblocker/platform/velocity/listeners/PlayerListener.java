package me.taucu.commandblocker.platform.velocity.listeners;

import com.google.common.eventbus.Subscribe;
import com.velocitypowered.api.event.connection.DisconnectEvent;
import me.taucu.commandblocker.util.TabMemory;

public class PlayerListener {

    @Subscribe
    public void onQuit(DisconnectEvent e) {
        TabMemory.MEMORY.remove(e.getPlayer().getUniqueId());
    }

}
